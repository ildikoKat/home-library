var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

mongoose.connect("mongodb://localhost/project", {poolSize:10}, function(err){
    if(err)
        console.log("Couldn't connect to mongodb");
});


var db = mongoose.connection;


var UserSchema = new mongoose.Schema({
    username: {type: String, required:true, unique: true},
    firstName : {type: String, required:true},
    lastName : {type: String, required:true},
    email:  {type: String, required:true},
    password :  {type: String, required:true},
    isAdmin: Boolean
});

var BookSchema = new mongoose.Schema({
    isbn: {type: Number, unique: true},
    title: {type: String, required:true},
    author : {type: String, required:true},
    inStock: {type: Number, requred: true},
    genre: [String],
    borrowers : [String],
});

var User =  mongoose.model('User', UserSchema);
var Book =  mongoose.model('Book', BookSchema);

function initUsers(){
    User.remove({},function(err, removed){});
    var newUser = new User({
        username: "admin",
        firstName : "Ildiko",
        lastName : "Katona",
        email: "email@yahoo.com",
        password : "Admin11",
        isAdmin : true
    });
      bcrypt.genSalt(10,function(err,salt){
          bcrypt.hash(newUser.password,salt,function(err,hashed){
              newUser.password = hashed;
              newUser.save();
          });
      });
}

// initUsers();

module.exports = {User,Book, initUsers};