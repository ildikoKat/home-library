var express = require('express');
var router = express.Router();

var {User, Book} = require('../utilities/databases/usersDB');
var bcrypt = require('bcryptjs');

function loggedIn(req,res,next){
  if(req.session && req.session.user){ //if someone is logged in
    return next();
  }else
  {
    res.render('login', {title:'Login'});
  }
}

//EDIT USER PERSONAL INFORMATION
router.post('/editUser', loggedIn,  function(req, res, next) {
  var username = req.session.user;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var email = req.body.email;
  var password = req.body.password;

  bcrypt.genSalt(10,function(err,salt){
    bcrypt.hash(password,salt,function(err,hashed){
      var query = {username:username}; 
      User.findOne(query,function(err,user) {
        if (err) 
          res.render('user',{title: 'Presonal user library', username:req.session.user, header:header, data: bookDatas, messages:'Something went wrong.Couldn\'t search set new data'});
        else {           
          user.firstName = firstName;
          user.lastName = lastName;
          user.password = hashed;

          user.save();
          res.redirect('/library');
        }
      });  
    });
  });
});

router.get('/updateFormData',loggedIn,  function(req,res,next){
  var resJSON = {};
  var query = {username:req.session.user};
  
  User.findOne(query,function(err, user){
    if(err){
      res.send(400,'Error user search');
    }else{
      res.type('json').send(user);
    }
  });
});

//FINDS THE BOOKS BASED ON THE SEARCH COMBOBOXES
router.post('/newBooksSelect',loggedIn, function(req,res,next){
  var title = req.body.title;
  var author = req.body.author;
  var genre = req.body.genre;
  var mine = req.body.mine;
  var query = {};
  if(title != "-"){
    query.title=title;
  }
  if(author != "-"){
    query.author = author;
  }
  if(genre!="-"){
    query.genre = genre;
  }

 if(mine==100){
    query.borrowers = req.session.user;
  }

  Book.find(query,function(err,foundBooks){
      if(err){
          res.send(400,"Error book search");
      }else{
          res.type('json').send(foundBooks);
      }            
  });
});

//CHECKS IF ARRAY CONTAINS ITEM
function contains(item, array){
  for(var i = 0; i< array.length; i++){
    if(array[i]==item){
      return true;
    }
  }
  return false;
}

function containsIndex(item, array){
  for(var i = 0; i< array.length; i++){
    if(array[i]==item){
      return i;
    }
  }
  return -1;
}

//IF SOMEONE BORROWS A BOOK CHECK IF THERE ARE STILL AVAILABLE AND ADDS THEM TO THE LIST
router.post('/borrowBook',loggedIn, function(req,res,next){
  var isbn = req.body.isbn;
  var query = {isbn:isbn}; 
  Book.findOne(query,function(error, book){
    if(error) res.render('error',{message:error,error:error});
    else{
      if(book){
        var borrowers = book.borrowers;
        var bookNr = book.inStock;
        var user = req.session.user;
        if(borrowers.length < bookNr && !contains(user, borrowers)){
          borrowers.push(user);
          book.borrowers = borrowers;
          book.save();
        }
      }
    }
  });
});

router.post('/returnBook',loggedIn, function(req,res,next){
  var isbn = req.body.isbn;
  var query = {isbn:isbn}; 
  Book.findOne(query,function(error, book){
    if(error) res.render('error',{message:error,error:error});
    else{
      if(book){
        console.log("found: "+book);

        var borrowers = book.borrowers;
        var user = req.session.user;
        var i =containsIndex(user,borrowers);
        if( i !=-1)
          borrowers.splice(i,1);
        book.borrowers = borrowers;
        book.save();
        console.log("after: "+book);

      }
    }
  });
});



module.exports = router;



