var express = require('express');
var router = express.Router();

var {User,Book} = require('../utilities/databases/usersDB');
var bcrypt = require('bcryptjs');


var header = ['ISBN','Title','Author','In Stock','Genre','Borrowers'];

// CHECKS IF SOMEONE IS LOGGED IN
function loggedIn(req,res,next){
  if(req.session && req.session.user){ //if someone is logged in
    return next();
  }else
  {
    res.render('login', {title:'Login'});
  }
}

// GETS THE DISTINCT DATA OF BOOKS (isbn, title, author, genre.. )
async function fillCombo(something){
  var result = await Book.distinct(something);
  if(result == null){
      res.send(400,"Error "+something+" search");
  }
  return result;
}

async function fillAllCombo(){
  const isbns = await fillCombo("isbn");
  const titles = await fillCombo("title");
  const authors = await fillCombo("author");
  const genres =  await fillCombo("genre");
  titles.unshift("-");
  authors.unshift("-");
  genres.unshift("-");
  return {isbns, titles, authors, genres};
}
//-------------------------------------------------------------------

// GET users listing.
router.get('/', loggedIn, function(req,res,next){ 
  
  //to fill the table with books form dtabase
  Book.find({}, function(err, books) {
    if(err){ 
        error.status = 404;
        res.render('error',{message:error,error:error});
    }
    var bookDatas = [];
    books.forEach(element => {
      bookDatas.push([element.isbn, element.title,element.author,element.inStock,element.genre,element.borrowers]);
    });

    fillAllCombo().then( booksDataCombo => {
      if(req.session.isAdmin){
        res.render('library',{title: 'Personal library', username:req.session.user, header:header, data: bookDatas, isbn: booksDataCombo.isbns});
      }else{
        res.render('user',{title: 'Personal user library', username:req.session.user, header:header, data: bookDatas, bookData: booksDataCombo});
      }
    }).catch(error => {
      res.render('error',{message:error,error:error});
    });
  });
});

//LOGOUT
router.get('/logout', function(req,res,next){
  userList=[];
  if(req.session){
    req.session.destroy(function(err){
      if(err){
        res.render('error',{message:error,error:error});
      }
    })
  }
  res.redirect('/');
})

//ADD BOOK ADMIN
router.post('/addBook',loggedIn,function(req,res,next){
  var isbn = req.body.isbn;
  var title = req.body.title;
  var author = req.body.author;
  var genre = req.body.genre;
  var inStock = req.body.stock;
  
  var newBook = new Book({
    isbn: isbn,
    title: title,
    author : author,
    inStock: inStock,
    genre: genre,
    borrowers : []
  });
  
  var query = {isbn:isbn}; //we need to check if isbn is taken
  Book.findOne(query,function(err,book){
    if (err) res.render('library',{title:'Personal Library',username:req.session.user, messages:'Something went wrong.Couldn\'t search isbn'});
      if(book){ 
        res.render('library',{title:'Personal Library',username:req.session.user, messages:'ISBN already exists'});
      }else{
      newBook.save();
      res.redirect('/library');
      }
  });
});

//DELETE BOOK ADMIN
router.post('/deleteBook',loggedIn,function(req,res,next){
  var isbn = req.body.isbnDel;
  var query = {isbn: isbn};
  Book.findOne(query,function(err,book){
    if (err) res.render('library',{title:'Personal Library',username:req.session.user, messages:'Something went wrong.Couldn\'t search isbn'});
    var bookNumber = book.inStock;
    if(bookNumber > 1){
      book.inStock--;
      book.save();
    }else{
      Book.deleteOne(query,function(err,result){
        if (err) res.render('library',{title:'Personal Library',username:req.session.user, messages:'Something went wrong.Couldn\'t search isbn'});
      });
    }
    res.redirect('/library');
  });
});



module.exports = router;