var express = require('express');
var router = express.Router();

var session = require('../app').session;
var User = require('../utilities/databases/usersDB').User;
var bcrypt = require('bcryptjs');

//CHECKS IF SOMEONE IS ALREADY LOGGED IN
function loggedIn(req,res,next){
    if(!req.session || !req.session.user){
      return next();
    }else
    {
        res.redirect('library');  //if someone is logged in redirrect them to the users
    }
}

/* GET users listing. */
router.get('/', loggedIn, function(req, res, next) {
  res.render('login',{title:'Login'});
});

router.post('/',function(req, res,next) {
    var username = req.body.username;
    var password = req.body.password;
    var query = {username: username}; //will be search based on username
    User.findOne(query,function(err,user){
        if(err) 
            res.render('login',{title: 'Login',messages:'Something went wrong. Couldn\'t search username'}); //if there was an error with findOne
        if(!user){//if there is no user with username
            res.render('login',{title: 'Login',messages:'Username doesn\'t exists'});
        }else{
            //compares the found user's pasword with the input password
            bcrypt.compare(password, user.password, function(err, matches) {
                if(err)
                    res.render('login',{title: 'Login', messages:'Something wrong.Could\'t check password'});
                if(matches){
                    req.session.user = username;
                    req.session.authenticated = true;
                    req.session.isAdmin = user.isAdmin;
                    res.redirect('library');
                }else{
                    res.render('login',{title: 'Login',messages:'Incorrect password'})   
                }
            });
        }
    });
});

module.exports = router;
