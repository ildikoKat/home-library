var express = require('express');
var router = express.Router();
var {s,s,initUsers} = require('../utilities/databases/usersDB')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home Library' });
});

router.get('/initUsers', function(req, res){
  initUsers();
  res.render('index', { title: 'Home Library' });
});
module.exports = router;