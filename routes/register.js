var express = require('express');
var router = express.Router();
var User = require('../utilities/databases/usersDB').User;
var bcrypt = require('bcryptjs');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('register',{title:'Registration'});
});

//REGISTER BUTTON CLICKED
router.post('/', function(req, res, next) {
  var username = req.body.username;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var email = req.body.email;
  var password = req.body.password;
  
  var newUser = new User({
      username: username,
      firstName : firstName,
      lastName : lastName,
      email: email,
      password : password,
      isAdmin : false
  });
  
      var query = {username:username}; //we need to check if username is taken
      User.findOne(query,function(err,user){
          if (err) res.render('register',{title:'Registration',messages:'Something went wrong.Couldn\'t search username'});
          if(user) 
              res.render('register',{title:'Registration',messages:'Username already exists'});
          else{
              bcrypt.genSalt(10,function(err,salt){
                  bcrypt.hash(password,salt,function(err,hashed){
                      newUser.password = hashed;
                      newUser.save();
                      res.redirect('/');
                  });
              });
          }
      });
});

module.exports = router;
