# Home Library

A web application project for Babes-Bolyai University web programming course.

The application is an online platform for a library where the admin can manage the available books and can see who borrowed them. A user has to register in order to borrow from the available books. The user can search the books based on their title, author and genre. The user can also return the books and this would be shown to the admin too. 

---
> **_NOTE:_**  I am fully aware that the application is not secure, it was made following some requirements and has room for lots of improvements.



---

## How to start
In a terminal in the project's folder install the modules with <code>npm install</code>

In the home directory run the <code>npm start</code> command to start the application.

The application is running on <code>http://localhost:3000 </code>

To initialize the users (deletes every user and inserts the admin) load <code>http://localhost:3000/initUsers</code>.


## Built With

Javascript, jQuery, handlebars, mongoDB, nodeJS

## Authors

* **Katona Ildiko-Noemi**
