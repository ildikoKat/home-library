function usernameCheck(){
    var name = document.getElementById("username");
    console.log(name.value);
    var length = name.value.length;
    if(!name.checkValidity() || length > 12){
        document.getElementById("messages").innerHTML = "Username should only contain letters and numbers and be max 12 characters long!"
        return false;
    }
    else{
        document.getElementById("messages").innerHTML = ""; 
        return true;           
    }
}

var passwordOk = true;

function passwordCheck(){
    var passwordElem = document.getElementById("password");
    var password = passwordElem.value;
    var lengthPass = password.length;
    if(!passwordElem.checkValidity() || lengthPass > 12 || lengthPass < 6){
        document.getElementById("messages").innerHTML = "Password length should be between 6 and 12";
        passwordOk = false;
        return false;
    }else{
        if (! /[A-Z]/.test(password) || ! /[0-9]/.test(password)) {
            document.getElementById("messages").innerHTML = "Password should contain a number and a capital letter";
            passwordOk = false;
            return false;        
        }
        else{
            document.getElementById("messages").innerHTML = "";
            passwordOk = true;
            if(document.getElementById("repassword")){
                passwordCheckEquality();
            }
            return true;
        }           
    }
}

function passwordCheckEquality(){
    var password = document.getElementById("password").value;
    var repasswordElem = document.getElementById("repassword");
    var repassword = repasswordElem.value;
    if(passwordOk){
        if(!repasswordElem.checkValidity() || repassword !== password){
            document.getElementById("messages").innerHTML = "The passwords don't match";
            return false;
        }else{
            document.getElementById("messages").innerHTML = "";
            passwordOk = true;
            return true;
        }
    }
}

function validateFunction(){
    var username = document.getElementById("username");
    var firstName = document.getElementById("firstName");
    var lastName = document.getElementById("lastName");
    var email = document.getElementById("email");
    var password = document.getElementById("password")
    var repassword = document.getElementById("repassword");
    
    if(!username.checkValidity() || !firstName.checkValidity() || !lastName.checkValidity() || !email.checkValidity() || !password.checkValidity() || !repassword.checkValidity() || document.getElementById("messages").innerHTML !=""){
        return false;
    }else{
        return true;
    }
}