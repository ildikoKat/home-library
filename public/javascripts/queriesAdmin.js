$(document).ready(function(){
    $("#showBook").click(function(){
        $("#showBookTable").show();
        $("#addBookForm").hide();
        $("#deleteBookForm").hide();
    });

    $("#addBook").click(function(){
        $("#addBookForm").show();
        $("#showBookTable").hide();
        $("#deleteBookForm").hide();

    });
    $("#deleteBook").click(function(){
        $("#deleteBookForm").show();
        $("#showBookTable").show();
        $("#addBookForm").hide();
    });

});