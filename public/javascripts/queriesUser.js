function updateTable(booksData,tableName,text){
    var tablazat = document.getElementById(tableName);
    tablazat.innerHTML = "<tr><th>ISBN</th><th>Title</th><th>Author</th><th>In Stock</th><th>Genre</th><th>Borrowers</th></tr>";
    booksData.forEach(element => {
        tablazat.innerHTML += "<tr><td>"+element.isbn+ "</td><td>"+element.title+ "</td><td>"+element.author+"</td><td>"+element.inStock+"</td><td>"
        +element.genre+"</td><td>"
        +element.borrowers+"</td>"
        +"<td><button class='"+text+"'>"+text+"</button></td></tr>";
    });
}

function sendPost(){
    $.post("/libraryUser/newBooksSelect",{
        title: document.getElementById('title').value,
        author: document.getElementById('author').value,
        genre: document.getElementById('genre').value,
        mine: 100
    },function(booksData,status){
        var tablazat = document.getElementById("tableMyBook");
        tablazat.innerHTML = "<tr><th>ISBN</th><th>Title</th><th>Author</th><th>In Stock</th><th>Genre</th><th>Borrowers</th></tr>";
        booksData.forEach(element => {
            tablazat.innerHTML += "<tr><td>"+element.isbn+ "</td><td>"+element.title+ "</td><td>"+element.author+"</td><td>"+element.inStock+"</td><td>" 
            +element.genre+"</td><td>" 
            +element.borrowers+"</td><td><button class=return>return</button></td></tr>";
        });
    });
}

$(document).ready(function(){
    $("#showBook").click(function(){
        $("#showBookTable").show();
        $("#showUserData").hide();
        $("#showMyBookTable").hide();
        $("#tableBookDiv").show();
        $("#tableMyBookDiv").hide();
    });

    $("#modifyUserData").click(function(){
        $("#showUserData").show();
        $("#showBookTable").hide();
        $.get("/libraryUser/updateFormData", function(data) {
           document.getElementById('username').value = data.username;
           document.getElementById('firstName').value = data.firstName;
           document.getElementById('lastName').value = data.lastName;
           document.getElementById('email').value = data.email;
        });
    });

    $("#myBooks").click(function(){
            $("#showUserData").hide();
            $("#showBookTable").show();
            $("#tableBookDiv").hide();
            $("#tableMyBookDiv").show();
            //sendPost();
    });


    $("#selectMyBooks").click(function(){
        sendPost();
    });

    $("#selectBooks").click(function(){
        $.post("/libraryUser/newBooksSelect",{
            title: document.getElementById('title').value,
            author: document.getElementById('author').value,
            genre: document.getElementById('genre').value,
            mine: 2
        },function(books,status){
            updateTable(books, 'tableBook', 'borrow');
        });
    });

    $(".borrow").click(function(){
        //closest- starts from this element and searches from anchestors for the first <tr> element
        // from this row it searches for the first <td> child
        var isbn = $(this).closest("tr").find("td:nth-child(1)").text();
        $.post("/libraryUser/borrowBook", {isbn:isbn});
    });

    $(".return").click(function(){
        var isbn = $(this).closest("tr").find("td:nth-child(1)").text();
        $.post("/libraryUser/returnBook", {isbn:isbn});
    });


});